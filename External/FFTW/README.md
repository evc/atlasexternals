FFTW
====

This package builds FFTW, a C subroutine library for computing the discrete
Fourier transform (DFT) in one or more dimensions.

It takes the source for the build from ftp://ftp.fftw.org/pub/fftw/, but
then applies a patch to it to replicate the version of the code that was
stored directly in SVN previously.
