# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building lwtnn for the offline
# builds.
#

# The name of the package:
atlas_subdir( lwtnn )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

if( ATLAS_BUILD_BOOST )
   set( Boost_INCLUDE_DIRS ${CMAKE_INCLUDE_OUTPUT_DIRECTORY} )
else()
   find_package( Boost 1.54.0 REQUIRED )
endif()

if( ATLAS_BUILD_EIGEN )
   set( EIGEN_INCLUDE_DIRS "${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/eigen3" )
else()
   find_package( Eigen 3.0.5 REQUIRED )
endif()

set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/lwtnnBuild )

# Build lwtnn for the build area:
ExternalProject_Add( lwtnn
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY https://github.com/lwtnn/lwtnn.git
   GIT_TAG v2.6
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DEIGEN3_INCLUDE_DIR:PATH=${EIGEN_INCLUDE_DIRS}
   -DBOOST_INCLUDEDIR:PATH=${Boost_INCLUDE_DIRS}
   LOG_CONFIGURE 1
   )
ExternalProject_Add_Step( lwtnn buildinstall
  COMMAND ${CMAKE_COMMAND} -E remove_directory
   ${_buildDir}/cmake
  COMMAND ${CMAKE_COMMAND} -E copy_directory
  ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing lwtnn into the build area"
  DEPENDEES install
  )
add_dependencies( Package_lwtnn lwtnn )

if( ATLAS_BUILD_EIGEN )
  add_dependencies ( lwtnn Eigen )
endif()

if( ATLAS_BUILD_BOOST )
   add_dependencies( lwtnn Boost )
endif()

# Install lwtnn:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES cmake/Findlwtnn.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
